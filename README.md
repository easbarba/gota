<!--
Gota is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gota is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Gota. If not, see <https://www.gnu.org/licenses/>.
-->

# Gota

Ansible, but for end-users!

## Usage

```sh
gota --pack
gota --distro --filter ubuntu
gota --misc --info systemctl
```

## Options

| Option            | Description                                |
|-------------------|--------------------------------------------|
| --distro, -d      | run configurations in the `distro` folder  |
| --pack, -p        | run configurations in the `pack` folder    |
| --misc, -m        | run configurations in the `misc` folder    |
| --filter TERM, -f | filter configurations matching `TERM` term |
| --refresh, -r     | refresh repositories before installing     |
| --upgrade, -u     | upgrade packages before installing         |
| --version, -v     | display version                            |
| --help, -h        | display help usage                         |

# Configurations

`Gota` provide by default, 3 actions:

## Distro

```toml
topic = "flatpak"
upgrade = "flatpak update"
install = "flatpak install flathub -y"

[packages]
tools = [ "io.github.NhekoReborn.Nheko" ]
apps = [
  "com.discordapp.Discord",
  "com.microsoft.Teams",
  "com.slack.Slack",
  "us.zoom.Zoom",
  "com.skype.Client"
]
```

## Pack

```toml
topic = "python"
deps = [ "python" ]
pre = "python3 -m pip install --upgrade pip"
install = "python3 -m pip install --upgrade --user"
link = "Builds/graal/bin"

[packages]
tools = [
  "black",
  "pipenv",
  "pyflakes",
  "pytest",
  "isort",
  "nose",
  "pyperclip",
  "pytaglib",
  "twine",
  "'python-lsp-server[all]'"
]
libs = [ "beautifulsoup4", "interval", "yt-dlp" ]
apps = [ "cryptography" ]

```

More examples of configurations at `EXAMPLES/*.json`.

## Misc

```toml
topic = "systemd"
install = "sudo systemctl enable --now"
deps = [ "systemctl" ]

[jobs]
servers = [ "nginx.service" ]
utils = [ "fstrim.timer" ]

```

More information on `gota --help`

## PROTOCOL

More information on `PROTOCOL.md`

# LICENSE

[GPL-v3](https://www.gnu.org/licenses/gpl-3.0.en.html)
