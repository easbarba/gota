#!/usr/bin/env python3
"""Packages installation made easy."""

# Gota is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gota is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gota. If not, see <https://www.gnu.org/licenses/>.

import os
import subprocess
import tomllib
import argparse
from pathlib import Path
import warnings
import shutil

symbol = "❯"
homeDir = Path.home()
localBinDir = homeDir.joinpath(".local", "bin")

# ================================= CORE


def systemRun(command: str) -> None:
    """Run system command."""
    if args.verbose:
        print(f"{symbol} {command}")

        subprocess.run(command, shell=True, executable="/bin/bash")


def greet(name: str) -> None:
    """Welcome user with initial message."""
    print("\n", f" -- {name} --", "\n")


# add commons operations
def common() -> None:
    """Do common operations of all commands."""
    warnings.warn("not implemented")


def checkdeps(list) -> None:
    """Check dependencies are available."""
    for exec in list:
        if not shutil.which(exec):
            print(f"{exec} Executable not found, exiting!")
            exit(1)


def dolist(commands) -> None:
    """Do list of command(s)."""
    if isinstance(commands, list):
        for command in commands:
            if args.verbose:
                print(f"{symbol} {command}")
            systemRun(command)
            return

    if isinstance(commands, str):
        if args.verbose:
            print(f"{symbol} {commands}")
        systemRun(commands)
        return


def routes(action: str):
    """Select command to be run."""
    commands = {"misc": misc, "distro": distro, "pack": pack}

    return commands[action]


def doCommand(item: str, data: any, where: str) -> None:
    """Just do it."""
    print("\n")
    if args.verbose:
        print(f"{symbol} {item}")

    choice = routes(where)
    choice(data)


def parse(rawdata: str):
    """Parse configuration files."""
    try:
        data = tomllib.load(rawdata)
    except Exception as ew:
        print("Error:\n", ew)
        exit(1)
    except tomllib.TOMLDecodeError as tew:
        print("Error in configuration found:\n", tew)
        exit(1)

    return data


def run(action: str) -> None:
    """Initiate running."""
    for item in found(action):
        if not os.path.exists(item):
            continue

        with open(item, "rb") as rawdata:
            data = parse(rawdata)

        doCommand(item, data, action)

    exit(0)


# ================================= MIDDLEWARE


def found(action: str):
    """List all found configuration files."""
    configs = homeDir.joinpath(".config", "gota", action)

    if not configs.is_dir():
        print("Configuration folder not found")
        exit(1)

    if not any(configs.iterdir()):
        print("Configuration folder is empty!")
        exit(1)

    configs = toml(configs.iterdir())

    if args.filter:
        return filtered(configs, args.filter)

    return configs


def toml(configs):
    """Return only toml configuration files."""
    return list(filter(lambda x: x.suffix == ".toml", configs))


def filtered(files, to_filter, verbose=False) -> list:
    """Filter that match filter provided by user."""
    files = list(filter(lambda x: x.name.startswith(to_filter), files))

    if verbose:
        print("Files found: ", list(map(lambda x: x.name, files)), "\n")

    return files


# ================================= COMMANDS


def pack(data: dict) -> None:
    """Language packages managers."""
    greet(data["topic"])
    checkdeps(data["deps"])

    if args.upgrade and "upgrade" in data.keys():
        dolist(data["upgrade"])

    # install all packages
    for key, packages in data["packages"].items():
        for pack in packages:
            if args.verbose:
                print(f"\n {symbol} {pack} \n")
            systemRun(f"{data['install']} {pack}")

            if "update" in data.keys():
                if args.verbose:
                    print("\n -- Updating -- ", pack, "\n")
                systemRun(f"{data['update']} {pack}")

            if "link" in data.keys():
                if args.verbose:
                    print(
                        f"\n -- Linking {homeDir.joinpath(data['link'], pack)} to {localBinDir.joinpath(pack)}  -- \n"
                    )

                localBinDir.joinpath(pack).symlink_to(
                    homeDir.joinpath(data["link"], pack)
                )

    # TODO warning in log no post has been provided
    # run post command
    if "post" in data.keys():
        dolist(data["post"])


def distro(data: dict) -> None:
    """Distro packages management."""
    greet(data["topic"])
    checkdeps(data["deps"])

    if args.refresh and "refresh" in data.keys():
        systemRun(data["refresh"])

    if "pre" in data.keys():
        dolist(data["pre"])

        # refresh again if additional repositories have been added
        if args.refresh and "refresh" in data.keys():
            systemRun(data["refresh"])

    if args.upgrade and "upgrade" in data.keys():
        systemRun(data["upgrade"])

    # install all packages
    for key, packages in data["packages"].items():
        for package in packages:
            if args.verbose:
                print(f"\n {package} \n")
            systemRun(f"{data['install']} {package}")

    if "post" in data.keys():
        dolist(data["post"])


def misc(data: dict) -> None:
    """Miscellaneous tasks."""
    greet(data["topic"])
    checkdeps(data["deps"])

    # refresh command
    if args.refresh and "refresh" in data.keys():
        systemRun(data["refresh"])

    if args.upgrade and "upgrade" in data.keys():
        systemRun(data["upgrade"])

    if args.verbose:
        print("\n", data["install"], "\n")

    for key, jobs in data["jobs"].items():
        for job in jobs:
            if args.verbose:
                print("\n", job, "\n")
            systemRun(f"{data['install']} {job}")

    # post command
    if "post" in data.keys():
        dolist(data["post"])


# ================================= CLI

parser = argparse.ArgumentParser(
    prog="Gota", description="Packages installation made easy!"
)
parser.add_argument("-p", "--pack", help="install user packages", action="store_true")
parser.add_argument(
    "-d", "--distro", help="install distro packages", action="store_true"
)
parser.add_argument(
    "-m", "--misc", help="install miscellaneous things", action="store_true"
)
parser.add_argument(
    "-r", "--refresh", help="referesh repository before installing", action="store_true"
)
parser.add_argument(
    "-u", "--upgrade", help="upgrade all packages before installing", action="store_true"
)
parser.add_argument("-f", "--filter", help="filter configurations to be run")
parser.add_argument(
    "-i", "--verbose", help="provide additional information", action="store_true"
)
parser.add_argument("-v", "--version", action="version", version="%(prog)s 0.1")
args = parser.parse_args()


# ================================= MAIN


def main():
    """Go for it."""
    try:
        if args.pack:
            run("pack")
        elif args.distro:
            run("distro")
        elif args.misc:
            run("misc")
        else:
            parser.print_help()
            exit(1)
    except KeyboardInterrupt:
        print("exiting!")
        exit(0)


if __name__ == "__main__":
    main()
